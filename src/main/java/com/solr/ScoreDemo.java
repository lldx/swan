package com.solr;

import com.utils.SearchUtil;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Jeff on 2018/7/3.
 */
public class ScoreDemo {
    private static final String SOLRSERVERURL = "http://127.0.0.1:8081/solr/song";
    HttpSolrClient SolrServer = new HttpSolrClient.Builder(SOLRSERVERURL).build();
    String keyWord = "*";
    //String scoreMethod = "sum(div(play_times,maxPlayTimes)^0.2+div(likes,maxLikes)^0.1)^maxscore";

    @Test
    public void select() throws IOException, SolrServerException {
       /* SolrQuery query = new SolrQuery();
        query.set("q", keyWord);
        //query.set("fl", "*,score");
        query.set("defType", "edismax");
        //query.set("bf", scoreMethod);
        ArrayList<Object> list = new ArrayList<>();
        query.addSort("id",SolrQuery.ORDER.desc);
        QueryResponse response = SolrServer.query(query);
        SolrDocumentList solrDocumentList = response.getResults();
        for (Object solrDoc : solrDocumentList) {
           System.out.println("desc: "+solrDoc);
        }
        System.out.println(solrDocumentList.toString());
        Float maxScore = solrDocumentList.getMaxScore();
        System.out.println(maxScore);*/
        String strKeyWord = "*";
        HashMap<String, Object> mapParams = new HashMap<>();
        mapParams.put("likes", "100");
        String strSortField = "id";
        String strSortDirection = "desc";
        Integer iStart = 0;
        Integer iLimit = 9;
        List list = SearchUtil.query(strKeyWord, mapParams, strSortField, strSortDirection, iStart, iLimit);
        for (Object o : list) {
            System.out.println("===:" + o);
        }
    }

    @Test
    public void testDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = dateFormat.format(new Date());
        System.out.println(format);
    }

    @Test
    public void add() throws IOException, SolrServerException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = dateFormat.format(new Date());
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", "1");
        map.put("name_py", "knf");
        map.put("likes", "10");
        map.put("play_times", "10");
        map.put("update_time", format);
        map.put("capture_time", new Date());

        UpdateResponse add = SolrServer.add(builderSolrInputDocument(map));
        System.out.println("add" + add);
        SolrServer.commit();
    }

    public static SolrInputDocument builderSolrInputDocument(Map<String, Object> program) {
        SolrInputDocument doc = new SolrInputDocument();
        Iterator<Map.Entry<String, Object>> it = program.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            doc.addField(entry.getKey(), entry.getValue());
        }
        return doc;
    }
}
