package com.file;

import com.utils.POIUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.List;

/**
 * Created by Jeff on 2018/8/15.
 */
public class test {
    //分块，也是线程数
    private static int blockCount = 4;

    public static void main(String args[]) {
        /*String src = "F:/test/file_00090fbe-492c-4418-a920-aaecdac1d17e_TS8M1080p.ts";
        String desc = "F:/test/1/file_00090fbe-492c-4418-a920-aaecdac1d17e_TS8M1080p.ts";
        long startTime = System.currentTimeMillis();
        File souceFile = new File(src);
        File targetFile = new File(desc);
        fastCopy(souceFile, targetFile);
        //long len = souceFile.length();
        //long oneNum = len / blockCount;
        //for (int i = 0; i < blockCount - 1; i++) {
        //    new CopyFileThread(src, desc, oneNum * i, oneNum * (i + 1)).start();
        //}
        //文件长度不能整除的部分放到最后一段处理
        //new CopyFileThread(src, desc, oneNum * (blockCount - 1), len).start();
        long endTime = System.currentTimeMillis();
        System.out.println(endTime - startTime + "ms");*/
        //String fileMD5 = GetFileName.getFileMD5(new File("F:\\test\\iGame__NO45_0.12.0.11021_Shipping_Google_GLOBAL.signed.shell_UAWebsite.apk"));
        //System.out.println(fileMD5);
        File file = new File("F:/test/test.xlsx");
        try {
            List<String[]> excel = POIUtil.readExcel(file);
            for (String[] str:excel){
                for (int i=0;i<str.length;i++){
                    System.out.println(str[i]);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void fastCopy(File source, File target) {
        FileOutputStream fos = null;
        FileInputStream fis = null;
        if (!source.exists() || !source.isFile()) {
            throw new IllegalArgumentException("file not exist!");
        }
        if (target.exists()) {
            target.delete();
        }
        try {
            target.createNewFile();
            fis = new FileInputStream(source);
            fos = new FileOutputStream(target);
            FileChannel fisChannel = fis.getChannel();
            WritableByteChannel fosChannel = fos.getChannel();
            fisChannel.transferTo(0, fisChannel.size(), fosChannel);
            fisChannel.close();
            fosChannel.close();
            fis.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
