package com.file;

import org.junit.Test;
import ws.schild.jave.EncoderException;
import ws.schild.jave.MultimediaInfo;
import ws.schild.jave.MultimediaObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Created by Jeff on 2018/8/15.
 */
public class GetFileName {
    public static void main(String[] args) {
        boolean NEEDMD5 = false;
        FileWriter fileWriter = null;
        //需要获取文件名称的目录
        String directionPath = args[0];
        //String directionPath = "F:\\test";
        //存放文件名称的文件名称
        String txtName = args[1];
        //String txtName = "F:\\test\\iptv\\1.txt";
        File file = new File(directionPath);
        if (args[2] != "" && args[2] != null) {
            NEEDMD5 = Boolean.parseBoolean(args[2]);
        }
        //NEEDMD5=true;
        int j=0;
        try {
            fileWriter = new FileWriter(txtName);
            String[] filelist = file.list();
            for (int i = 0; i < filelist.length; i++) {
                if (NEEDMD5) {
                    fileWriter.write(filelist[i] + "=" + getFileMD5(new File(directionPath + "/" + filelist[i])) + "\r\n"); // 换行写入txt文件中
                } else {
                    fileWriter.write(filelist[i] + "\r\n");
                }
                j++;
                System.out.println(j+"/"+filelist.length+"  "+filelist[i]);
            }
            System.out.println("OK，It's End!!!!");
            // 注意关闭的先后顺序，先打开的后关闭，后打开的先关闭
            //fileWriter.flush();
            //fileWriter.close();
        } catch (Exception e) {
        } finally {
            try {
                fileWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //String fileMD5 = getFileMD5(new File("F:/test/file_0672b347-5fd8-43ad-b6cf-71c4392a566f_TS4M1080p.ts"));
        //System.out.println(fileMD5);
        //getDuration();
    }

    @Test
    public static void getDuration() {
        try {
            MultimediaObject multimediaObject = new MultimediaObject(new File("F:/test/file_0672b347-5fd8-43ad-b6cf-71c4392a566f_TS4M1080p.ts"));
            MultimediaInfo info = multimediaObject.getInfo();
            long duration = info.getDuration();
            System.out.println(duration);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getFileMD5(File file) {
        if (!file.exists() || !file.isFile()) {
            return null;
        }
        MessageDigest digest = null;
        FileInputStream in = null;
        byte buffer[] = new byte[1024];
        int len;
        try {
            digest = MessageDigest.getInstance("MD5");
            in = new FileInputStream(file);
            while ((len = in.read(buffer, 0, 1024)) != -1) {
                digest.update(buffer, 0, len);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }
}
