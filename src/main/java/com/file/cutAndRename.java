package com.file;

import com.utils.POIUtil;
import org.junit.Test;

import javax.swing.plaf.PanelUI;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Jeff on 2018/8/15.
 */
public class cutAndRename {
    public static void main(String args[]) throws IOException {
        Date startTime = new Date();
        String xmlPath = args[0];
        //String xmlPath = "D:\\test\\11.xlsx";
        String srcPath = args[1];
        //String srcPath = "D:\\test";
        String descPath = args[2];
        //String descPath = "D:\\test\\test";
        //File srcFile = new File(srcPath);
        //File descFile = new File(descPath);
        int j = 0;
        File excelFile = new File(xmlPath);
        List<String[]> excelList = POIUtil.readExcel(excelFile);
        ArrayList<Object> unExistFileList = new ArrayList<>();
        for (String[] arr : excelList) {
            File srcFile = new File(srcPath + "/" + arr[0]);
            File descFile = new File(descPath + "/" + arr[1]);
            if (!srcFile.exists()) {
                unExistFileList.add(arr[0]);
            } else {
                cutAndRename(srcFile, descFile);
                ++j;
                System.out.println(arr[0] + "     success< > " + j);
            }
        }
        Date endTime = new Date();
        long useTime = (endTime.getTime() - startTime.getTime()) / 1000;
        System.out.println("************************* GAME OVER **************************** ALL USER " + useTime + "s");
        System.out.println("************************* NOT  FIND **************************** " + unExistFileList.size());
        for (Object s : unExistFileList) {
            System.out.println(s);
        }
    }

    public static boolean cutAndRename(File srcFile, File descFile) {
        boolean flag = false;
        try {
            if (srcFile.isDirectory()) {
                String srcPath = srcFile.getAbsolutePath();
                srcPath = srcPath.replaceAll("\\\\", "/");
                String descPath = descFile.getAbsolutePath();
                descPath = descPath.replaceAll("\\\\", "/");
                // 复制目的文件夹
                if (!descFile.exists()) {
                    descFile.mkdir();
                }
                int lastIndexOf = srcPath.lastIndexOf("/");
                //获取剪切目的文件夹的路径
                descPath = descPath + srcPath.substring(lastIndexOf, srcPath.length());
                descPath = descPath.replaceAll("//", "/");
                File newDescFile = new File(descPath);
                // 遍历文件夹
                for (File f : srcFile.listFiles()) {
                    cutAndRename(f, newDescFile);
                }
            } else {
                if (descFile.isDirectory()) {
                    String srcPath = srcFile.getAbsolutePath();
                    srcPath = srcPath.replaceAll("\\\\", "/");
                    String descPath = descFile.getAbsolutePath();
                    descPath = descPath.replaceAll("\\\\", "/");
                    int lastIndexOf = srcPath.lastIndexOf("/");
                    descPath = descPath + srcPath.substring(lastIndexOf, srcPath.length());
                    // 写文件
                    descPath = descPath.replaceAll("//", "/");
                    File newDescFile = new File(descPath);
                    flag = srcFile.renameTo(newDescFile);
                } else {
                    flag = srcFile.renameTo(descFile);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    //剪切文件或者文件夹
    //TODO
    public void cut(File srcFile, File descFile) {
        try {
            if (!srcFile.exists()) {
                return;
            }
            if (!descFile.exists()) {
                if (descFile.isDirectory()) {
                    descFile.mkdirs();
                } else {
                    String absolutePath = descFile.getAbsolutePath();
                    int lastIndexOf = absolutePath.lastIndexOf("/");
                    String substring = absolutePath.substring(lastIndexOf, absolutePath.length());
                }
            }
            if (srcFile.isDirectory()) {
                File[] listFiles = srcFile.listFiles();
                for (File f : listFiles) {
                    if (f.isFile()) {
                        File newDescFile = new File(descFile.getAbsolutePath() + "/" + f.getName());
                        srcFile.renameTo(newDescFile);
                    } else if (f.isDirectory()) {
                        //f://a/aa  f://b/aa
                        cut(f, new File(descFile.getAbsolutePath() + "/" + f.getName()));
                    }
                }

            } else {
                srcFile.renameTo(descFile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() {
        String srcPath = "F:\\test\\iptv";
        String descPath = "F:\\test\\1";
        File file = new File(srcPath);
        File file1 = new File(descPath);

        cutAndRename(file,file1);
    }

}
