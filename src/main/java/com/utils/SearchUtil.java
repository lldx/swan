package com.utils;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Jeff on 2018/7/3.
 */
public class SearchUtil {

    private static CloudSolrClient cloudSolrClient;
    private static HttpSolrClient httpSolrClient;

    private static final String SOLR_SERVERURL = "http://127.0.0.1:8081/solr/song";
    private static final String zkHost = "192.168.1.50:2181,192.168.1.51:2181,192.168.1.52:2181";
    private static final String DEFAULT_COLLECTION = "program";
    private static final Integer ZkClientTimeout = 10000;
    private static final Integer ZkConnectTimeout = 10000;


    public static CloudSolrClient getClient1() {
        cloudSolrClient = new CloudSolrClient.Builder().withZkHost(zkHost).build();
        cloudSolrClient.setDefaultCollection(DEFAULT_COLLECTION);
        cloudSolrClient.setZkClientTimeout(ZkClientTimeout);
        cloudSolrClient.setZkConnectTimeout(ZkConnectTimeout);
        return cloudSolrClient;
    }

    private static HttpSolrClient getClient() {
        httpSolrClient = new HttpSolrClient.Builder(SOLR_SERVERURL).build();
        return httpSolrClient;
    }

    public static List query(String strKeyWord, Map<String, Object> mapParams, String strSortField, String strSortDirection,
                             Integer iStart, Integer iLimit) throws IOException, SolrServerException {
        SolrQuery query = new SolrQuery();
        query.set("q", strKeyWord);
        //query.set("defType", "edismax");
        //query.set("bf", scoreMethod);
        ModifiableSolrParams params = new ModifiableSolrParams();
        for (Map.Entry<String, Object> entry : mapParams.entrySet()) {
            if (null != entry.getValue() && !"".equals(entry.getValue())) {
                query.addFilterQuery(entry.getKey() + ":" + entry.getValue());
            }
        }
        query.addSort(strSortField, "desc".equalsIgnoreCase(strSortDirection) ? SolrQuery.ORDER.desc : SolrQuery.ORDER.asc);
        query.setStart(iStart);
        query.setRows(iLimit);
        params.add(query);
        HttpSolrClient client = getClient();
        QueryResponse response = client.query(query);
        SolrDocumentList docs = response.getResults();
        long numFound = docs.getNumFound();
        Float maxScore = docs.getMaxScore();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        for (SolrDocument solrDoc : docs) {
            list.add(solrDoc);
        }
        return list;
    }

    public static void updateIndex(Map<String, Object> mapInputDocument) throws IOException, SolrServerException {
        HttpSolrClient client = getClient();
        getClient().add(builderSolrInputDocument(mapInputDocument));
        client.commit();
    }

    public static void deleteById(String id) throws IOException, SolrServerException {
        HttpSolrClient client = getClient();
        client.deleteById(id);
        client.commit();
    }

    public static List<Object> queryById(String id) throws IOException, SolrServerException {
        SolrQuery query = new SolrQuery();
        query.set("q", id);
        query.set("df", "id");
        HttpSolrClient client = getClient();
        QueryResponse response = client.query(query);
        SolrDocumentList solrDocumentList = response.getResults();
        List<Object> list = new ArrayList<>();
        for (Object solrDoc : solrDocumentList) {
            list.add(solrDoc);
        }
        return list;
    }

    public static SolrInputDocument builderSolrInputDocument(Map<String, Object> document) {
        SolrInputDocument doc = new SolrInputDocument();
        Iterator<Map.Entry<String, Object>> it = document.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Object> entry = it.next();
            doc.addField(entry.getKey(), entry.getValue());
        }
        return doc;
    }
}
