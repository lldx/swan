package cn.jeff;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 * Created by Jeff on 2018/6/12.
 */
@WebServlet(name = "checkName", urlPatterns = "/checkName")
public class checkName extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            request.setCharacterEncoding("utf-8");
            //response.setContentType("text/html;charset=utf-8");
            PrintWriter writer = response.getWriter();
            String username = request.getParameter("username");
            System.out.println(username+"-----");
            if (username == null || username.equals("")) {
                writer.write("-3");
            } else if (username.equals("jeff")) {
                writer.write("1");
            } else if (username.equals("忙")) {
                writer.write("-2");
            } else if (username.equals("123")) {
                writer.write("-1");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
