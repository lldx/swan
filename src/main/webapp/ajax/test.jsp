<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/6/11
  Time: 23:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script type="text/javascript">
        //创建Ajax引擎对象的方法
        function getxmlHttp() {
            var xmlhttp;
            //判断浏览器版本
            if (window.XMLHttpRequest) {
                //创建ajax引擎对象
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                //创建ajax引起对象
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            return xmlhttp;
        }

        //获取到ajax核心引擎对象
        // alert(getxmlHttp());
        function ajax1() {
            var xhr = getxmlHttp();
            xhr.open("GET", "/swan/demo1?username=" + Math.random(), true);
            // xhr.setRequestHeader("ContentType","")
            xhr.send();
        }

        // ajax1();
        function ajax2() {
            var xhr = getxmlHttp();
            xhr.open("GET", "/swan/demo1?username=111", true);
            xhr.send();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    var _data = xhr.responseText;
                    document.getElementById("mydiv").innerText = _data;
                }
                // else {
                //     alert("ajax响应错误！");
                // }
            }

        };

        function ajax3() {
            var xhr = getxmlHttp();
            xhr.open("POST", "/swan/demo1", true);
            xhr.setRequestHeader("ContentType", "application/x-www-form-urlencoded");
            xhr.send("username=jeff")
        }

        function myFunction() {
            var x;
            var txt = "";
            var person = {fname: "Bill", lname: "Gates", age: 56};
            for (x in person) {
                txt = txt + person[x];
                alert(txt);
            }
            document.getElementById("demo").innerHTML = txt;
        }

        function demo1() {
            setTimeout(function () {
                alert("hello")
            }, 3000);
        }

        //循环定时
        var c = 0;
        var t;
        var timer_is_on = 0;

        function demo2() {
            // document.getElementById("demo").value=c;
            document.getElementById("demo").innerHTML = c;
            c++;
            t = setTimeout("demo2()", 1000);
        }

        function doTimer() {
            if (!timer_is_on) {
                timer_is_on = 1;
                demo2();
            }
        }

        function stopCount() {
            clearTimeout(t);
        }

        //时钟
        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById("clock").innerHTML = h + ":" + m + ":" + s;
            setTimeout(function () {
                startTime();
            }, 500);
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        //创建对象
        var song = {
            name: "惊鸿一面",
            sing: "许嵩",
            chorus: "黄龄",
            info: function () {
                //document.write(this.name + "---" + this.sing);
                return this.name + "---" + this.sing;
            }

        }
        function template(name, age) {
            this.name = name;
            this.age = age;
        }

        tem = new template("许嵩","33");
        document.write(tem.name+"---11---"+tem.age);




        window.onload=function () {
            startTime();
            document.getElementById("demo4").innerHTML = song.info()
        }
    </script>

</head>
<body>
<div id="mydiv"></div>
<button type="button" onclick="ajax3()">修改内容</button>

<form action="/swan/demo1" method="post">
    <input type="text" name="username">
    <input type="submit" value="提交">
</form>
<button onclick="myFunction()">测试</button>
<br>
<button onclick="demo1()">定时</button>
<br>
<button id="demo2" onclick="demo2()">循环定时</button>
<br>
<button id="demo3" onclick="stopCount()">停止循环</button>
<br>
<p id="demo"></p>
<p id="demo4"></p>
<div id="clock"></div>
</body>
</html>
