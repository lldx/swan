<%--
  Created by IntelliJ IDEA.
  User: liulang
  Date: 2018/6/12
  Time: 15:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script>
        function getxmlHttp() {
            var xmlhttp;
            //判断浏览器版本
            if (window.XMLHttpRequest) {
                //创建ajax引擎对象
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            } else {
                //创建ajax引起对象
                // code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            return xmlhttp;
        }

        function _check() {
            var _un = document.getElementById("_un");
            var _username = _un.value;
            var _msg = document.getElementById("_msg");
            var _ff = document.getElementById("_ff");
            var xhr = getxmlHttp();
            // xhr.open("GET", "/swan/checkName?username=" + _username, true);
            xhr.open("POST", "/swan/checkName", true);
            // xhr.open("POST", "/swan/demo1", true);
            xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            // xhr.setRequestHeader("ContentType", "multipart/form-data");
            xhr.send("username=" + _username)
            // xhr.send();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    var _data = xhr.responseText;
                    // var obj = JSON.parse(xhr.responseText);
                    // alert(_data);
                    if (_data === "1") {
                        _msg.innerHTML = "用户名可用";
                        _ff.onsubmit = function () {
                            return true;
                        }
                    }
                    else if (_data === "-1") {
                        _msg.innerHTML = "用户名重复";
                        _ff.onsubmit = function () {
                            return false;
                        }
                    }
                    else if (_data === "-2") {
                        _msg.innerHTML = "服务器忙";
                        _ff.onsubmit = function () {
                            return false;
                        };
                    }
                    else if (_data === "-3") {
                        _msg.innerHTML = "用户名不能为空";
                        _ff.onsubmit = function () {
                            return false;
                        };
                    } else {
                        _ff.onsubmit = function () {
                            return false;
                        }
                    }
                }
            }
        }

    </script>
    <style type="text/css">
        #_msg {
            color: red;
            font-family: 微软雅黑;
            size: A4;
        }
    </style>
</head>
<body>
<form id="_ff" action="/swan/demo1" method="post">
    <!-- 用来提示用户错误信息 -->
    输入用户名：<input id="_un" type="text" name="username" onblur="_check();"><span id="_msg"></span><br>
    <input type="submit" value="提交">
</form>

</body>
</html>
